# Zabbix 报警 发送至微信

#### 介绍
使用python进行将zabbix的报警输出到企业微信

#### 软件架构
配合zabbix监控平台及企业微信使用


#### 安装教程

1. 将文件拷贝到  _/usr/lib/zabbix/alertscripts/_  目录下，并设置权限为 _-rwxrwxrwx 1 zabbix zabbix_ 
（`chmod  777 sendmail.sh` 
`chown  zabbix:zabbix sendmail.sh`）
2. 配置corp_id 、corp_secret 、agent_id、token
3. 配置zabbix报警媒介类型为 _脚本_ ，名称为 _sendmail.sh_ 
4. 设置动作为 _发送消息给用户: Admin (Zabbix Administrator) 通过  python2wechat_   脚本参数分别为 _{ALERT.SENDTO}{ALERT.SUBJECT}{ALERT.MESSAGE}_ 
![报警媒介](./img/V%400182926%7D_I%7D6HXM%7BGWNXV.png "媒介设置")
5. 使用命令sed -i 's/\r$//' sendmail.sh 可以解决 > sh: /bin/sh^M: 坏的解释器: 没有那个文件或目录
_ 
#### 使用说明

1. 正常使用zabbix 即可
2. 依赖：`pip install requests `
3. 安装pip（安装epel扩展源：　`yum -y install epel-release` 安装`pip yum -y install python-pip`）(升级pip  `pip install --upgrade pip`)

